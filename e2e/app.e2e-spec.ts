import { browser, element, by } from 'protractor';

describe('Water for the Future', () => {

  beforeEach(() => {
    browser.get('');
  });

  it('should have {nav}', () => {
    element(by.css('ion-navbar')).isPresent().then(present => expect(present).toEqual(true));
  });

  it('has a menu button that displays the side menu', () => {
    element(by.css('.bar-button-menutoggle')).click()
      .then(() => {
        browser.driver.sleep(2000); // wait for the animation
        element.all(by.css('.toolbar-title img')).first().getAttribute('alt').then(att => expect(att).toEqual('Água para o Futuro'));
      });
  });

  it('the menu should have login button', () => {
    expect(element(by.css('ion-menu ion-toolbar ion-item ion-label *')).getTagName().then(tag => expect(tag).toEqual('button')));
  });

  it('the left menu has a link with title springs', () => {
    element(by.css('.bar-button-menutoggle')).click()
      .then(() => {
        browser.driver.sleep(2000); // wait for the animation
        element.all(by.css('ion-menu ion-content ion-item ion-label')).first().getText().then(text => expect(text).toEqual('Nascentes'));
      });
  });

});

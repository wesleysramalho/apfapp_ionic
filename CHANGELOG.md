# 3.6.4 (2018-05-16)

### Features

* **iOS**: Build corrections


# 3.6.1 (2018-05-06)

### Features

* **iOS**: Build corrections


# 3.6.0 (2018-05-02)

### Features

* **App**: Image recognition update [8eafcb3](https://bitbucket.org/unemidias/apfionic/commits/8eafcb36bb937cf2e64f83e91bac779f0768b48f)
* **App**: Facebook Cordova plugin update [2dbba16](https://bitbucket.org/unemidias/apfionic/commits/2dbba160b4c69b0dc8c4e4f81ff6f02911c076aa)


# 3.5.5 (2018-04-11)

### Features

* **App**: Image recognition updated [f2c4cc8](https://bitbucket.org/unemidias/apfionic/commits/f2c4cc817e98c5ba7678ac699697d2fc15a524b8)
* **App**: Monitoring text correction [c9bde4e](https://bitbucket.org/unemidias/apfionic/commits/c9bde4ee48ad9efe578ee67aadcda19a93772e46)
* **App**: New Spring text correction [d934780](https://bitbucket.org/unemidias/apfionic/commits/d9347809ba0520157232b2cb626461e27ea34169)
* **App**: New home page and GSAP for dom elements animations [e04f43c](https://bitbucket.org/unemidias/apfionic/commits/e04f43c0331bdcca1c66dfa0b8845cb82ce03e4c)


# 3.5.1 (2018-03-21)

### Features

* **App**: Augmented reality Wikitude Plugin installed and first version with recognition of the first public process with AR and projects manual [d67b799](https://bitbucket.org/unemidias/apfionic/commits/d67b79990129c6f14899655b1df86e3819603402)


# 3.1.5 (2018-03-05)

### Features

* **App**: StatusBar setting done in the HomeComponent view cycle [ffbfcfa](https://bitbucket.org/unemidias/apfionic/commits/ffbfcfa41ea193982751d070f4d33e17457fbd02)


# 3.1.4 (2018-03-05)

### Features

* **App**: Verify NavController element id in production to set the statusbar color [873e9d7](https://bitbucket.org/unemidias/apfionic/commits/873e9d7ebf429d87965e227c7d75e118677f284d)


# 3.1.3 (2018-03-05)

### Features

* **App**: Correct the title of some pages [c823ea0](https://bitbucket.org/unemidias/apfionic/commits/c823ea0df4b17f1c666ef0dd83219dd01d62d8d7)
* **App**: Correct nav path to login and register [17346ad](https://bitbucket.org/unemidias/apfionic/commits/17346ad7df8c9841605bdfe3b5e502a80efd18a4)


# 3.1.2 (2018-03-04)

### Features

* **App**: Change the statusbar style on Home [ec295b0](https://bitbucket.org/unemidias/apfionic/commits/ec295b008000d03f6d719c574b99f58a8eef8d29)


# 3.1.1 (2018-03-02)

### Features

* **iOS**: Update cordova-ios to version 4.5.4 [adb137e](https://bitbucket.org/unemidias/apfionic/commits/adb137e91741ae9e1f015516218a9a7fda9de9ac)
* **iOS**: Update resources [c29a1d1](https://bitbucket.org/unemidias/apfionic/commits/c29a1d1a993899214a75a52ba2852a1fe705deb8)


# 3.1.0 (2018-02-28)

### Features
* **App**: Angular 5 and Ionic-native modules and plugins have been updated (2acd11d)
* **App**: Unit test configuration using angular-cli and jasmine/karma (2b9752f)
* **App**: E2E test configuration using angular-cli and protractor (b60fff4)
* **App**: Update ng2-translate to @ngx-translate (4a728b3)
* **Cordova**: Cordova iOS, Cordova Android and severel Cordova plugins update (021e615)


# 3.0.7 (2018-02-27)

### Features

* **iOS**: Correct iOS platform configuration to only iPhone target


# 3.0.6 (2018-02-23)

### Features

* **App**: Timers to prevent app to get stuck in loading screem
* **IONIC**: Ionic-CLI update


# 3.0.5 (2018-02-11)

### Features

* **App**: Correction to location model


# 3.0.1 (2017-09-21)

### Features

* **App**: Removed location component and set location to allways auto


# 3.0.0 (2017-09-08)

### Features

* **App**: Location page and component to set up location on the map
* **App**: New layout to home page
* **App**: About page
* **App**: Water masses and water courses map overlays
* **GMAPS**: Change the Google Maps API


# 2.2.4 (2017-04-20)

### Features

* **App**: New images and iOS/Android resorces
* **iOS**: Several small changes to iOS deploy


# 2.0.0 (2017-04-20)

### Features

* **App**: Full password recover functions
* **App**: Cancel button for facebook login to avoid errors
* **App**: Better error handling for user registrations
* **App**: Email form validation


# 1.0.0 (2017-02-18)

### Features

* **App**: iOS and Android Platforms added
* **App**: Final text and titles
* **App**: Corrections and code changes


# 0.6.0 (2017-01-27)

### Features

* **App**: Monitoring page
* **App**: Legal text
* **App**: Corrections and code changes


# 0.5.0 (2017-01-24)

### Features

* **App**: New home page style
* **App**: New side menu style


# 0.4.0 (2017-01-23)

### Features

* **App**: Ionic action sheet
* **App**: Contact and about pages


# 0.3.0 (2017-01-24)

### Features

* **App**: New Spring
* **App**: Custon Icons


# 0.2.0 (2017-01-22)

### Features

* **App**: Springs map
* **App**: Springs details


# 0.1.0 (2017-01-20)

### Features

* **App**: User login
* **App**: New user registration


# 0.0.1 (2017-01-19)

### Features

* **App**: Skeleton app using `ionic2App`

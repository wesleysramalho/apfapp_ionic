## App Project Água para o Futuro

The app Project Water for the Future is an hybrid app made with Ionic Framework V2+ for Android and IOS with the objective of identify, preserve and educate the population about the natural water springs in the urban area of Cuiabá MT.

The project seeks to protect the water springs of the urban area in Cuiabá, in order to ensure hydric safety and potable water supply for the present and the future generations.

The Water for the Future is a joint initiative between the Mato Grosso’s State Public Ministry, the Ação Verde Institute and the Federal University of Mato Grosso. The project seeks, first and foremost, to ensure water security of the urban area of Cuiabá and the future supply of drinking water through identification, preservation and recovery of water springs. With this aspiration, a multidisciplinary technical team of specialists composed of geologists, hydrogeologists, forest engineers, sanitary engineers, biologists, remote-sensing specialists, among other professionals, are engaged in works in the field, analysis, researches and other diverse scientific activities (identifying, characterizing, monitoring, etc;) directly connected to the project, forecast in its plan of action.

[See more about the projct in the official website](https://aguaparaofuturo.mpmt.mp.br)


## Install & Start

You need to be running [the latest node LTS](https://nodejs.org/en/download/) or newer

```bash
git clone https://proartti@bitbucket.org/unemidias/apfionic.git
cd apfionic
npm install
ionic serve         # start the application on dev server
```

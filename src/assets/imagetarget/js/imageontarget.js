var World = {
	loaded: false,

	init: function initFn() {
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {
		/*
			First an AR.ImageTracker needs to be created in order to start the recognition engine. It is initialized with a AR.TargetCollectionResource specific to the target collection that should be used. Optional parameters are passed as object in the last argument. In this case a callback function for the onTargetsLoaded trigger is set. Once the tracker loaded all its target images, the function worldLoaded() is called.
			Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
			Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.
		*/
      this.targetCollectionResource = new AR.TargetCollectionResource("https://www.tiagodonatti.net/temp/apfapp/tracker.wtc", {});
      this.tracker = new AR.ImageTracker(this.targetCollectionResource, {
          onTargetsLoaded: this.removeLoadingBar,
          onError: function(errorMessage) {
            alert(errorMessage);
          },
          onDisabled: () => {
            console.log('disable1');

            AR.platform.sendJSONObject({action: "disabled"});
          }
      });
		/*
			The next step is to create the augmentation. In this example an image resource is created and passed to the AR.ImageDrawable. A drawable is a visual component that can be connected to an IR target (AR.ImageTrackable) or a geolocated object (AR.GeoObject). The AR.ImageDrawable is initialized by the image and its size. Optional parameters allow for position it relative to the recognized target.
		*/
		/* Create overlay for page one */
    var imgOne = new AR.ImageResource("https://www.tiagodonatti.net/temp/apfapp/icon.png");
    var frogModel = new AR.Model("https://www.tiagodonatti.net/temp/apfapp/frog3.wt3", {
      rotate: {
        x: 0,
        y: 0,
        z: 180
      },
      scale: {
        x: 0.03,
        y: 0.03,
        z: 0.03
      },
      translate: {
        x: -0.004489620876963307,
        y: -0.319801139258872,
        z: -0.00690881081064082
      }
    });
		var overlayOne = new AR.ImageDrawable(imgOne, 1, {});
		/*
			The last line combines everything by creating an AR.ImageTrackable with the previously created tracker, the name of the image target and the drawable that should augment the recognized image.
			Please note that in this case the target name is a wildcard. Wildcards can be used to respond to any target defined in the target collection. If you want to respond to a certain target only for a particular AR.ImageTrackable simply provide the target name as specified in the target collection.
    */

    // this.clientTracker = new AR.ClientTracker("assets/tracker.wtc", {
    //   onLoaded: this.loadingStep
    // });

   var manualCover = new AR.Trackable2DObject(this.tracker, "manual_capa", {
      drawables: {
        cam: [frogModel]
      },
      onError: function(errorMessage) {
        alert(errorMessage);
      }
    });

		var page6 = new AR.ImageTrackable(this.tracker, "pag6", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideopag6,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var page71 = new AR.ImageTrackable(this.tracker, "pag7-1", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideopag7,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var page8 = new AR.ImageTrackable(this.tracker, "pag8", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideopag8,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
    });
		var manualF01 = new AR.ImageTrackable(this.tracker, "manual-fig01", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF07 = new AR.ImageTrackable(this.tracker, "manual-fig07", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF08 = new AR.ImageTrackable(this.tracker, "manual-fig08", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF14 = new AR.ImageTrackable(this.tracker, "manual-fig14", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF15 = new AR.ImageTrackable(this.tracker, "manual-fig15", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF27 = new AR.ImageTrackable(this.tracker, "manual-fig27", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF28 = new AR.ImageTrackable(this.tracker, "manual-fig28", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF29 = new AR.ImageTrackable(this.tracker, "manual-fig29", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF30 = new AR.ImageTrackable(this.tracker, "manual-fig30", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF43 = new AR.ImageTrackable(this.tracker, "manual-fig43", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF46 = new AR.ImageTrackable(this.tracker, "manual-fig46", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF50 = new AR.ImageTrackable(this.tracker, "manual-fig50", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
		var manualF51 = new AR.ImageTrackable(this.tracker, "extra_01", {
			drawables: {
				cam: overlayOne
			},
			onImageRecognized: this.requestVideoManual,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
		});
    var page6 = new AR.ImageTrackable(this.tracker, "pag6", {
      drawables: {
        cam: overlayOne
      },
      onImageRecognized: this.requestVideopag6,
      onError: function(errorMessage) {
        alert(errorMessage);
      }
    });
	},

	removeLoadingBar: function() {
		if (!World.loaded) {
			// var e = document.getElementById('loadingMessage');
      // e.parentElement.removeChild(e);
      document.getElementById('loadingMessage').innerHTML = '<button onclick="javascript: closeWikiPlugin();"></button>';
			World.loaded = true;
		}
	},

	worldLoaded: function worldLoadedFn() {

		document.getElementById('loadingMessage').innerHTML = '<button onclick="javascript: closeWikiPlugin();"></button>';
  },
  requestVideopag6: function() {
    AR.platform.sendJSONObject({action: "openvideo-pag6"});
  },
  requestVideopag7: function() {
    AR.platform.sendJSONObject({action: "openvideo-pag7-1"});
  },
  requestVideopag8: function() {
    AR.platform.sendJSONObject({action: "openvideo-pag8"});
  },
  requestVideoManual: function(e) {
    AR.platform.sendJSONObject({action: "openvideo-" + e.name});
  }
};
function closeWikiPlugin() {
  AR.platform.sendJSONObject({action: "closeWikitudePlugin"});
}

World.init();

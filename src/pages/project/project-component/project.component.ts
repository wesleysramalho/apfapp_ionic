import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import { Projectervice } from '../../../app/services/project.service';
import { EnvVariables } from '../../../app/environment-variables/environment-variables.token';

@Component({
  selector: 'page-project',
  templateUrl: 'project.html'
})
export class ProjectComponent {

  constructor(
    private aboutService: Projectervice,
    public navCtrl: NavController,
    @Inject(EnvVariables) public envVar
  ) {}


  private rootUrl = this.envVar.url;
  private imgUrl = this.rootUrl + '/media/k2/items/cache/';

  showImage:boolean = true;
  rawResult:any;
  about = {
    'id': '66',
    'title': '',
    'introtext': '',
    'fulltext': ''
  }

	ngOnInit(){
    this.aboutService.getAbout().subscribe(
      response => this.rawResult = response,
      error => console.log(error),
      () => {
        this.about.title = this.rawResult.params.title;
        this.about.introtext = this.rawResult.params.introtext;
        this.about.fulltext = this.changeSrc(this.rawResult.params.fulltext);
      }
    );

    this.loadItemImage(this.about.id);

  }

  loadItemImage(id){
    let itemImage = Md5.hashStr("Image"+id)+"_M.jpg"
    let myImage = new Image();
    myImage.src = this.imgUrl+itemImage;
    myImage.onerror = ()=> {
      this.showImage = false;
    }
    this.about["image"] = myImage;
  }

  changeSrc(str) {
    let elmtVar = str;
    return elmtVar.replace(/src=\"/g, 'src=\"' + this.rootUrl + '/' );
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { ProjectComponent } from './project-component/project.component';

import { Projectervice } from '../../app/services/project.service';

@NgModule({
  declarations: [
    ProjectComponent
  ],
  imports: [
  	CommonModule,
  	SharedModule
  ],
  exports: [
    ProjectComponent
  ],
  entryComponents:[
  	ProjectComponent
  ],
  providers: [
    Projectervice
  ]
})
export class ProjectModule {}

import { Component, OnInit, Inject } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import { EnvVariables } from '../../../app/environment-variables/environment-variables.token';
import { SpringsService } from '../../../app/services/springs.service';

@Component({
  templateUrl: 'springs-details.html'
})
export class SpringsDetailsComponent implements OnInit  {

  showImage:boolean = true;
  itemId:any;
  rawResult:any;
  spring = {
    'title': '',
    'introtext': '',
    'fulltext': ''
  }

  private rootUrl = this.envVar.url;
  private imgUrl = this.rootUrl + '/media/k2/items/cache/';

	constructor (
		public navCtrl: NavController,
    public alertCtrl: AlertController,
    public params: NavParams,
    private springsService: SpringsService,
    @Inject(EnvVariables) public envVar
  ){}

	ngOnInit(){
    this.itemId = this.params.get('id');

    this.springsService.springDetail(this.itemId).subscribe(
      response => this.rawResult = response,
      error => console.log(error),
      () => {
        this.spring.title = this.rawResult.params.title;
        this.spring.introtext = this.rawResult.params.introtext;
        this.spring.fulltext = this.changeSrc(this.rawResult.params.fulltext);
      }
    );

    this.loadItemImage(this.itemId);

  }

  loadItemImage(id){
    let itemImage = Md5.hashStr("Image"+id)+"_M.jpg"
    let myImage = new Image();
    myImage.src = this.imgUrl+itemImage;
    myImage.onerror = ()=> {
      this.showImage = false;
    }
    this.spring["image"] = myImage;
  }

  changeSrc(str) {
    let elmtVar = str;
    return elmtVar.replace(/src=\"/g, 'src=\"' + this.rootUrl + '/' );
  }
}

import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { NavController, AlertController, Platform, LoadingController } from 'ionic-angular';
import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';
import { SpringsService } from '../../../app/services/springs.service';
import { SpringsDetailsComponent } from '../springs-details-component/springs-details.component';
import { LocationService } from '../../../app/services/location.service';
import { LocationModel, CurrentLatLng } from '../../../app/models/location.model';
import { SpringItem, shapesSettings } from '../../../app/models/spring.model';

@Component({
  templateUrl: 'springs.html'
})
export class SpringsComponent implements OnInit  {

  allSprings: SpringItem[] = [];
  rawResult: any;
  currentIcon = "assets/icon/current_location_24.png";
  locationPos = <LocationModel>{};
  curLatLng = <CurrentLatLng>{};
  map: any;
  showMap = false;

  spLocation: string = "maplocation";
  snippet: string = 'This is <b>HTML</b>';

  setShapes = false;
  shapesObject = <shapesSettings>{};

  mapLoading: any;

	constructor (
    private loadingCtrl: LoadingController,
    private googleMaps: GoogleMapsAPIWrapper,
    private geolocation: Geolocation,
    private locationService: LocationService,
		private springsService: SpringsService,
		public alertCtrl: AlertController,
    public navCtrl: NavController,
    public plt: Platform
  ) {}

	ngOnInit() {

    this.mapLoading = this.loadingCtrl.create({
      content: "Carregando o mapa",
      dismissOnPageChange: true
    });

    this.locationService.location().then(
      ( value ) => {
        this.mapLoading .present();
        setTimeout(() => {
          this.mapLoading.dismiss();
        }, 10000);
        this.locationPos = value;
        this.curLatLng.lat = this.locationPos.lat;
        this.curLatLng.lng = this.locationPos.lng;
        this.showMap = true;
      },
      ( error ) => this.navCtrl.popToRoot()
    )

    this.shapesObject.shapeWaterCourse = false;
    this.shapesObject.shapeWaterMass = false;
    this.springsService.setShapes().subscribe(
      (res) => {
        if (!res.error) {
          this.shapesObject.shapeWaterCourseLink = res.params.shapeWaterCourseLink;
          this.shapesObject.shapeWaterMassLinks = res.params.shapeWaterMassLinks;
          this.setShapes = true;
        }
      }
    );

    this.springList();
  }

	springList() {
    this.springsService.getSprings().subscribe(
      response => this.rawResult = response,
      error => console.log(error),
      () => {
        if( this.rawResult.error == false ) {
          for( let spr of this.rawResult.params ){
            let spring = <SpringItem>{
              id: Number( spr.id ),
              title: spr.title,
              featured: Number( spr.featured ),
              district: spr.extra_fields2.bairro,
              stream: spr.extra_fields2.corrego,
              icon: spr.extra_fields2.icone,
              lat: Number( spr.extra_fields2.latitude ),
              lng: Number( spr.extra_fields2.longitude ),
              location: spr.extra_fields2.localizacao,
              subbasin: spr.extra_fields2.subbacia
            };
            this.allSprings.push( spring );
          }

        } else {
          let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Falha ao conectar no servidor. Por favor tente novamente mais tarde. (msg: ' + this.rawResult.msg + ')',
            buttons: ['OK']
          });
          alert.present();
        }
      }
    );
  }

  getIcon(iconNum) {
    let icon:string;
    if(iconNum){
      switch(iconNum){
        case'1':
          icon = 'assets/img/icon_blue.png';
          break;
        case'2':
          icon = 'assets/img/icon_yellow.png';
          break;
        case'3':
          icon = 'assets/img/icon_red.png';
          break;
        default:
          icon = 'assets/img/icon_blue.png';
          break;
      }
      return icon;
    } else {
      icon = 'assets/img/icon_blue.png';
    }
    return icon;
  }

  pushSpringDetails(itemId) {
    this.navCtrl.push( SpringsDetailsComponent, { id: itemId });
  }

  currentLocation() {
    this.curLatLng.lat = this.curLatLng.lat + -0.0000000001;
    this.curLatLng.lng = this.curLatLng.lng + -0.0000000001;

    if ( this.locationPos.auto ) {
      this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(( position ) => {
        this.curLatLng.lat = position.coords.latitude;
        this.curLatLng.lng = position.coords.longitude;
        this.googleMaps.panTo( this.curLatLng );
      });
    } else {
      this.googleMaps.panTo( this.curLatLng );
    }

  }

  paddingAdjust() {
    if(this.plt.is('ios')) {
      return 'padding';
    } else {
      return '';
    }
  }

  toogleShapeWaterCourse() {
    if (this.shapesObject.shapeWaterCourse) {
      this.shapesObject.shapeWaterCourse = false;
    } else {
      this.shapesObject.shapeWaterCourse = true;
    }
  }

  toogleShapeWaterMass() {
    if (this.shapesObject.shapeWaterMass) {
      this.shapesObject.shapeWaterMass = false;
    } else {
      this.shapesObject.shapeWaterMass = true;
    }
  }

  mapReady() {
    this.googleMaps.panTo( this.curLatLng );
    this.mapLoading.dismiss();
  }

}

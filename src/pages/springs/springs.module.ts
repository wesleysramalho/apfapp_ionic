import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { SpringsComponent } from './springs-component/springs.component';
import { SpringsDetailsComponent } from './springs-details-component/springs-details.component';
import { AgmCoreModule } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';
import { MarkerManager } from '@agm/core/services/managers/marker-manager';

// import { GoogleMaps } from '@ionic-native/google-maps';

import { SpringsService } from '../../app/services/springs.service';


@NgModule({
  declarations: [
    SpringsComponent,
    SpringsDetailsComponent
  ],
  imports: [
  	CommonModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'SEU_APP_KEY_AQUI'
    })
  ],
  exports: [
    SpringsComponent,
    SpringsDetailsComponent
  ],
  entryComponents:[
  	SpringsComponent,
    SpringsDetailsComponent
  ],
  providers: [
    // GoogleMaps,
    SpringsService,
    GoogleMapsAPIWrapper,
    MarkerManager
  ]
})
export class SpringsModule {}

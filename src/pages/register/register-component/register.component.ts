import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController, Platform, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';

import { UserService } from '../../../app/services/user.service';
import { AuthenticationService  } from '../../../app/services/authentication.service';

import { LoginComponent } from '../../login/login-component/login.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.html'
})
export class RegisterComponent implements OnInit {

  model: any = {};
  dataVar: any;

  constructor(
      public platform: Platform,
      public loadingCtrl: LoadingController,
      public navCtrl: NavController,
      public alertCtrl: AlertController,
      private menu: MenuController,
      private storage: Storage,
      private authenticationService: AuthenticationService,
      private userService: UserService,
      private fb: Facebook
    ) {}

  register() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.userService.create(this.model).subscribe(
        data => this.dataVar = data,
        error => {
          let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Erro ao enviar dados.'+error,
            buttons: [{
              text: 'OK',
              handler: data => {
                loader.dismiss();
              }
            }]
          });
          loader.dismiss();
          alert.present();
        },
        () => {
          if (this.dataVar.error == true) {
            let alert = this.alertCtrl.create({
              title: 'Erro',
              subTitle: 'Erro ao criar o usuário - ' + this.dataVar.msg,
              buttons: [{
                text: 'OK',
                handler: data => {
                  loader.dismiss();
                }
              }]
            });
            loader.dismiss();
            alert.present();
          } else {
            let alert = this.alertCtrl.create({
              title: 'Sucesso',
              subTitle: 'Usuário criado com sucesso',
              buttons: [{
                text: 'OK',
                handler: data => {
                  loader.dismiss();
                  this.goToLogin();
                }
              }]
            });
            loader.dismiss();
            alert.present();
          }
        }
      );
  }

  ngOnInit() { }

  goToLogin() {
    this.navCtrl.push(LoginComponent);
  }

  fbLogin() {

    let alertStart = this.alertCtrl.create({
      title: 'Aguarde',
      subTitle: 'Conectando com o facebook..',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: data => {
          this.navCtrl.popToRoot();
        }
      }]
    });
    alertStart.present();

    this.platform.ready().then(() => {
      //FB authentication
      this.fb.login(["email","public_profile"]).then((result) => {

        //Call FB API to get user name and email
        this.fb.api("/me?fields=name,email", null)
          .then((success)=> {

              // ///Call API to check if user exist, if not create user and return authenticated data
              this.authenticationService.fbLogin(success.email, success.name).subscribe(
                response => this.dataVar = response,
                error => {
                  alertStart.dismiss();
                  let alert = this.alertCtrl.create({
                    title: 'Erro',
                    subTitle: 'erro ao realizar o login' + error,
                    buttons: ['OK']
                  });
                  // loader.dismiss();
                  alert.present();
                },
                () => {
                  if(this.dataVar.error == false && this.dataVar.params.code != ''){
                    let userObj = {
                      "id": this.dataVar.params.code,
                      "name": this.dataVar.params.name,
                      "email": this.dataVar.params.email
                    }
                    this.storage.set('currentUser', userObj);
                    this.userService.setAppUser(this.dataVar.params.name, this.dataVar.params.email);

                    this.menu.enable(true);
                    alertStart.dismiss();

                    this.navCtrl.popToRoot();
                  } else {
                    let alert = this.alertCtrl.create({
                      title: 'Erro',
                      subTitle: 'Usuário ou senha incorretos',
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                });


          }, function (error) {
            console.log("error", error);
        });
      });
    })
  }

}



import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { RegisterComponent } from './register-component/register.component';

@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
  	CommonModule,
  	SharedModule
  ],
  exports: [
    RegisterComponent
  ],
  entryComponents:[
  	RegisterComponent
  ]
})
export class RegisterModule {}

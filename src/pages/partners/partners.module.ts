import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { PartnersComponent } from './partners-component/partners.component';

import { PartnersService } from '../../app/services/partners.service';

@NgModule({
  declarations: [
    PartnersComponent
  ],
  imports: [
  	CommonModule,
  	SharedModule
  ],
  exports: [
    PartnersComponent
  ],
  entryComponents:[
  	PartnersComponent
  ],
  providers: [
    PartnersService
  ]
})
export class PartnersModule {}

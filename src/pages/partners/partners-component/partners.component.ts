import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import { EnvVariables } from '../../../app/environment-variables/environment-variables.token';
import { PartnersService } from '../../../app/services/partners.service';

@Component({
  selector: 'page-partners',
  templateUrl: 'partners.html'
})
export class PartnersComponent {

  constructor(
    private partnersService: PartnersService,
    public navCtrl: NavController,
    @Inject(EnvVariables) public envVar
  ) {}


  private rootUrl = this.envVar.url;
  private imgUrl = this.rootUrl + '/media/k2/items/cache/';

  showImage:boolean = true;
  rawResult:any;
  partners = {
    'id': '67',
    'title': '',
    'introtext': '',
    'fulltext': ''
  }

	ngOnInit(){
    this.partnersService.getPartners().subscribe(
      response => this.rawResult = response,
      error => console.log(error),
      () => {
        this.partners.title = this.rawResult.params.title;
        this.partners.introtext = this.rawResult.params.introtext;
        this.partners.fulltext = this.changeSrc(this.rawResult.params.fulltext);
      }
    );

    this.loadItemImage(this.partners.id);

  }

  loadItemImage(id){
    let itemImage = Md5.hashStr("Image"+id)+"_M.jpg"
    let myImage = new Image();
    myImage.src = this.imgUrl+itemImage;
    myImage.onerror = ()=> {
      this.showImage = false;
    }
    this.partners["image"] = myImage;
  }

  changeSrc(str) {
    let elmtVar = str;
    return elmtVar.replace(/src=\"/g, 'src=\"' + this.rootUrl + '/' );
  }

}

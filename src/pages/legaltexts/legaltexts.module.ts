import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { PrivacypolicyComponent } from './privacypolicy-component/privacypolicy.component';
import { TermsofuseComponent } from './termsofuse-component/termsofuse.component';

import { LegaltextService } from '../../app/services/legaltext.service';

@NgModule({
  declarations: [
    PrivacypolicyComponent,
    TermsofuseComponent
  ],
  imports: [
  	CommonModule,
  	SharedModule
  ],
  exports: [
    PrivacypolicyComponent,
    TermsofuseComponent
  ],
  entryComponents:[
  	PrivacypolicyComponent,
    TermsofuseComponent
  ],
  providers: [
    LegaltextService
  ]
})
export class LegaltextsModule {}

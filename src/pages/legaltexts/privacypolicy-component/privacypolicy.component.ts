import { Component, Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import { EnvVariables } from '../../../app/environment-variables/environment-variables.token';
import { LegaltextService } from '../../../app/services/legaltext.service';

@Component({
  templateUrl: 'privacypolicy.html'
})
export class PrivacypolicyComponent {

  constructor(
    private legaltextService: LegaltextService,
    public navCtrl: NavController,
    @Inject(EnvVariables) public envVar
  ) {}

  private rootUrl = this.envVar.url;
  private imgUrl = this.rootUrl + '/media/k2/items/cache/';


  showImage:boolean = true;
  rawResult:any;
  legalText = {
    'id': '290',
    'title': '',
    'introtext': '',
    'fulltext': ''
  }

	ngOnInit(){
    this.legaltextService.getLegal().subscribe(
      response => this.rawResult = response,
      error => console.log(error),
      () => {
        this.legalText.title = this.rawResult.params.title;
        this.legalText.introtext = this.rawResult.params.introtext;
        this.legalText.fulltext = this.changeSrc(this.rawResult.params.fulltext);
      }
    );

    this.loadItemImage(this.legalText.id);

  }

  loadItemImage(id){
    let itemImage = Md5.hashStr("Image"+id)+"_M.jpg"
    let myImage = new Image();
    myImage.src = this.imgUrl+itemImage;
    let thisClass = this;
    myImage.onerror = function(){
      thisClass.showImage = false
    }
    this.legalText["image"] = myImage;
  }

  changeSrc(str) {
    let elmtVar = str;
    return elmtVar.replace(/src=\"/g, 'src=\"' + this.rootUrl + '/' );
  }

}

/// <reference path="../../../app/WikitudePlugin.d.ts" />
// <reference path="../../../../node_modules/architect_dts/architect.d.ts" />
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { VideoPlayerComponent } from '../videoplayer/videoplayer.component';

declare var WikitudePlugin: WikitudePlugin;

@Component({
  selector: 'page-imagerecognition',
  templateUrl: 'imagerecognition.html'
})
export class ImagerecognitionComponent {

  tracker: any;
  targetCollectionResource: any;
  unregisterBackButtonAction: any;
  msgText = 'Aguarde enquanto verificamos se o seu aparelho é compatível';

  androidKey = 'UdtL+9wFSsjL4Ek0cbeKXRAPCss4pR8Qe6Z0EDjnULJIfC3UQ+AJ926f/RhwJIo2SKU4I2JyFXnRJQPeLW/VD8xm8Jc1ZVCohm+i11drDg4lPr/7D9sPxXJeWmVlpSuXY1lRdJaAPGXsK1LxMDOgnwNEmN12H2wlTT1WAJ9az9dTYWx0ZWRfX9fK/zHUqtVuJZEcvGtF7eOwpoXxb7er3r94/GYWGc7fbGG7fiG/DhPoWRWja94kwpYVZs7VEpABrIqlNF8EsXUARbMx7NSpDA50qQFBUxKRVgex3svbUOH2e/pWDUod88icnRtF7WCO8hll6Uo+S8386PBppU3jbNNN0W1Gw/IeKVX0Xk3DTyuq+eUAUPjGMEV9LhAeY7Qm1PYPBYIQFKmgzTDIqcA82vem8xbXwsoUsq/nlUGhLflqJBlltPzSvyXouafEUAAfeHrNBYEJaYmu8bfRdEeRquaLJkAJh+pE2HtlaeCspKbz7oGKusaYF5Q9Y/Vzq4IgyG56stKXxImqiqq939hw88rsA3znSLiL6z8AK/IEAuQrwFFjhu//JE/LDFDFKPq7YPfm9gLO3DcPDJuGUNvhqbG5wXDgvKhFvWJV2UrBSsYtJeHe1UMY10ynLfCxScvmha4jK/IC8Sh7xh3GEtpaISL8KIyU25tl54/K0kudL5mP5AUfZO5RelxBCaHeNc8K';
  iosKey = '0HoqZ5MuiBc3g5FHyf7R2ybuxcrcQ8lE3zjraG2YooF/pRC6WCCE4FWjucCIM73YPbsSBdvIBNTPgko2721NU2oVnTF5n8jfGTRKIiZoxZawAsUhoFJSNLfRv1IFS7qIbSHxVS3uorunahxYA/ZsAwwtYP/22xsm/IZ4RPxJrdNTYWx0ZWRfX+ostcfDrc7CU3cwYqbLmUwsmfvzhCjNq6GwjGna2gsDw8uVc3JU5whB49Dx6Ik7jR6zvQGt8wg4MAmA/Ku1T4i5ziitqhIJVM/lP9mPxDl/DmY2lXEF4dAhxM3w971t4WPpTRp3bdCt56zHJlg66NcE+fURjIFNR0VGPslvzIYJc7WOC4jBEHmMNJt+mqi2+viC0Yzae9DtwgTsqarSBhumGPNedVFtfuDFCDCfV7XM0FRwcCPT3NMn8bWZ37DHgJ5n0SsJRSjQmtJy3MYBoW2JBFgSGBJMxZVemsCDP8/3+HAgwqhZvU6A1wUREpJl4L8WWgqQZ9t8GHCZc6z8p5oraAj0RjVIqhMNYknx2XsADLDOZqnV3mLNjOjHS6p783QAJouwcuaLWp2a26n7WdRSCU8z/K8gG9QYOuit/sJKP+bkt3IHkI2mbC+4tJ9I4hUOdwVigSB1b4sH4IyiqSn7O0EA34ApW9yU4HPJLD2lvQXZ+fn9kgw=';

  constructor(
    public navCtrl: NavController,
    // public youtube: YoutubeVideoPlayer
  ) {}

  ngOnInit(){}

  ionViewDidEnter() {

    this.startWikitudePlugin();

  }

  startWikitudePlugin() {
    let startupConfiguration: any = {"camera_position": "back"};
    // COLOCAR A RESPECTIVACHAVE PARA O BUILD
    WikitudePlugin._sdkKey = this.androidKey;

    WikitudePlugin.isDeviceSupported(
      (success) => {
        this.msgText = 'Sucesso! Aguarde enquanto as imagens estão sendo preparadas';
      },
      (fail) => {
        this.msgText = 'Falha. Seu aparelho celular parece não ser compatível com as funcionalidades de Realidade Ampliada. Em caso de dúvidas entre em contato com o projeto através dos canais de comunicação. ' + fail;
      },
      [WikitudePlugin.FeatureImageTracking]
    );

    WikitudePlugin.setJSONObjectReceivedCallback(obj => {

      console.log("setJSONObjectReceivedCallback ..."+JSON.stringify(obj));
        // this an example of how to receive a call from a function in the Wikitude SDK (Wikitude SDK --> Ionic)
        if (obj["action"]){
          switch (obj["action"]) {
            case "closeWikitudePlugin":
                // close wikitude plugin
                WikitudePlugin.close();
                this.navCtrl.popToRoot();
                break;
            case "captureScreen":

              WikitudePlugin.captureScreen(
                (absoluteFilePath) => {
                  // console.log("snapshot stored at:\n" + absoluteFilePath);

                  // this an example of how to call a function in the Wikitude SDK (Ionic2 app --> Wikitude SDK)
                  // WikitudePlugin.callJavaScript("World.testFunction('Screenshot saved at: " + absoluteFilePath +"');");
                },
                (errorMessage) => {
                  console.log(errorMessage);
                },
                true, null
              );

              break;
            case "openvideo-pag6":
              this.openVideo('ouaqXd1lzDY');
              // this.youtube.openVideo('ouaqXd1lzDY');
              break
            case "openvideo-pag7-1":
              this.openVideo('kWzd1yL39hc');
              break
            case "openvideo-pag8":
              this.openVideo('C1iZs_ewTnA');
              break

            case "openvideo-manual-fig01":
              this.openVideo('CeIoU4pkolw');
              break
            case "openvideo-manual-fig07":
              this.openVideo('CeIoU4pkolw');
              break
            case "openvideo-manual-fig08":
              this.openVideo('urBv2jAvJWI');
              break
            case "openvideo-manual-fig14":
              this.openVideo('BcnCzu9qoss');
              break
            case "openvideo-manual-fig15":
              this.openVideo('wTlsn4Z0oZc');
              break
            case "openvideo-manual-fig27":
              this.openVideo('OGYcyMcwo5Y');
              break
            case "openvideo-manual-fig28":
              this.openVideo('SUiZfMgy5KY');
              break
            case "openvideo-manual-fig29":
              this.openVideo('KTYV8cuPehs');
              break
            case "openvideo-manual-fig30":
              this.openVideo('V_veHExtIk0');
              break
            case "openvideo-manual-fig43":
              this.openVideo('1mdoOiGIe88');
              break
            case "openvideo-manual-fig46":
              this.openVideo('phLppddKnAg');
              break
            case "openvideo-manual-fig50":
              this.openVideo('f2XoSRFoCW4');
              break
            case "openvideo-extra_01":
              this.openVideo('olkkxmMsHWw');
              break
            case "disabled":
              this.navCtrl.popToRoot();
              break
            default:
              console.warn("action not handled => ", obj);
              break;
          } // end switch
        } // end if (obj.action)
    });

    WikitudePlugin.loadARchitectWorld(
      function(success) {
        console.log("ARchitect World loaded successfully.");
      },
      function(fail) {
        console.log("Failed to load ARchitect World!");
      },
      "www/assets/imagetarget/index.html",
      ["ir"],
      <JSON>startupConfiguration
    );

    //////////////////
    // WikitudePlugin.setBackButtonCallback(
    //   () => {
    //     // console.log("Back button has been pressed...");
    //     this.navCtrl.popToRoot();
    //   }
    // );
  }

  openVideo(videoID: string) {
    WikitudePlugin.close();
    setTimeout(() => {
      this.navCtrl.push(VideoPlayerComponent, {id: videoID})
    }, 1000);

    // this.navCtrl.push(VideoPlayerComponent, {id: videoID})
    // this.msgText = 'Abrindo o vídeo...'
    // this.showBtn = true;
    // setTimeout(() => {
    //   this.youtube.openVideo(videoID);
    // }, 1500);

  }

  onDeviceSupported() {
    console.log('onDeviceSupported');
  }

  onDeviceNotSupported() {
    console.log('onDeviceNotSupported');
  }

  ionViewWillLeave() {
    console.log(' Will leave')
  }

  ionViewWillEnter() {
    console.log(' Will enter')
  }
}

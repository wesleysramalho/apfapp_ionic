import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

@Component({
  selector: 'page-videoplayer',
  templateUrl: 'videoplayer.html'
})
export class VideoPlayerComponent {

  vidID: string;

  constructor(
    public navCtrl: NavController,
    public youtube: YoutubeVideoPlayer,
    private navParams: NavParams
  ) {
    this.vidID = this.navParams.get('id')
    this.openVideoPlayer(this.vidID);
  }

  ngOnInit(){}

  private openVideoPlayer(vidID: string) {
    this.youtube.openVideo(this.vidID);
  }

}

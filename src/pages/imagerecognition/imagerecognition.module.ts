import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { ImagerecognitionComponent } from './imagerecognition-component/imagerecognition.component';
import { VideoPlayerComponent } from './videoplayer/videoplayer.component';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';


@NgModule({
  declarations: [
    ImagerecognitionComponent,
    VideoPlayerComponent
  ],
  imports: [
  	CommonModule,
    SharedModule
  ],
  exports: [
    ImagerecognitionComponent,
    VideoPlayerComponent
  ],
  entryComponents:[
    ImagerecognitionComponent,
    VideoPlayerComponent
  ],
  providers: [
    YoutubeVideoPlayer
  ]
})
export class ImagerecognitionModule {}

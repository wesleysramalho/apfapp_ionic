import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';
import { MarkerManager } from '@agm/core/services/managers/marker-manager';

import { NewspringsComponent } from './newspring-component/newspring.component';
import { CustomlocationComponent } from './customlocation-component/customlocation.component';

import { NewspringsService } from '../../app/services/newsprings.service';

@NgModule({
  declarations: [
    NewspringsComponent,
    CustomlocationComponent
  ],
  imports: [
  	CommonModule,
  	SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'SEU_APP_KEY_AQUI'
    })
  ],
  exports: [
    NewspringsComponent,
    CustomlocationComponent
  ],
  entryComponents:[
  	NewspringsComponent,
    CustomlocationComponent
  ],
  providers: [
    NewspringsService,
    GoogleMapsAPIWrapper,
    MarkerManager
  ]
})
export class NewspringsModule {}

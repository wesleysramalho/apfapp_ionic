import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Content, NavController, AlertController, LoadingController, ActionSheetController  } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';
import { HomeComponent } from '../../home/home-component/home.component';
import { LoginComponent } from '../../login/login-component/login.component';
import { TermsofuseComponent } from '../../legaltexts/termsofuse-component/termsofuse.component';
import { PrivacypolicyComponent } from '../../legaltexts/privacypolicy-component/privacypolicy.component';
import { CustomlocationComponent } from '../customlocation-component/customlocation.component';

import { NewspringsService } from '../../../app/services/newsprings.service';
import { LocationService } from '../../../app/services/location.service';

import { LocationModel } from '../../../app/models/location.model';
import { NewSpringModel } from '../../../app/models/spring.model';

@Component({
	templateUrl: 'newspring.html',
	providers: [
		Camera
	]
})
export class NewspringsComponent implements OnInit  {

  model = <NewSpringModel>{};
  incognito: boolean;
  allSprings = [];
  rawResult: any;
  imgPreview: any = '';
  imgThumb: any = '';

  showKey = 0;

  locationMsg: string = 'Localização Atual';
  custonLocation = CustomlocationComponent;
  autoPos = true;
  ctmPos = false;
  termsOfUse = TermsofuseComponent;
  privacyPolicy = PrivacypolicyComponent;
  legalCheck: boolean = false;
  loginCheck: boolean;
  locationPos = <LocationModel>{};

  timer: any;

  loaderBox = this.loadingCtrl.create({
    content: "Aguarde..."
  });
  alertBox = this.alertCtrl.create({
    subTitle: 'Ainda estamos trabalhando. Por favor aguarde...',
    buttons: [{
      text: 'Cancelar envio',
      handler: data => {
        this.alertBox.dismiss();
        this.cancelSend();
      }
    }]
  });

  @ViewChild( Content ) content: Content;

  constructor (
    private sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,
    private newspringsService: NewspringsService,
    private locationService: LocationService,
    private camera: Camera,
    private storage: Storage,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public navCtrl: NavController
  ) {}

	ngOnInit() {

    this.model.autoPosition = this.autoPos;
    this.model.incognito = false;

    this.locationService.location().then(
      ( location ) => {

        this.locationPos = location;
        this.model.lat = this.locationPos.lat;
        this.model.lng = this.locationPos.lng;

        const admArea = this.locationPos.administrativeArea.toLowerCase();
        let admAreaCheck = /mato grosso|mt/.test(admArea);

        if (!admAreaCheck) {
          let alert = this.alertCtrl.create({
            title: 'Conteúdo bloqueado',
            message: 'O envio de novas nacentes só está disponível para as Cidades credenciadas no Projeto Água para o futuro. Por favor utilize a opção monitoramento.',
            buttons: [{
              text: 'Fechar',
              handler: () => {
                  this.navCtrl.setRoot( HomeComponent );
                }
              }],
            enableBackdropDismiss: false
          });
          alert.present();
        }

      }
    );

    this.storage.get( 'currentUser' ).then(( value ) => {
      if (value) {
        this.model.userId = value.id;
        this.model.userName = value.name;
        this.model.userEmail = value.email;
        this.model.name = value.name;
        this.model.email = value.email;
        this.showKey = 1;
      } else {
        this.showKey = 0;
      }
    });

  }

	ionViewWillEnter() {
		 this.springLocation();
	}

	ionViewWillLeave() {
		this.storage.get( 'springLocation' ).then(( value ) => {
			if ( value ) {
				this.storage.remove( 'springLocation' );
			}
		});
	}

	sendData(){
    this.loaderBox.present();
    this.timer = setTimeout(() => {
      this.loaderBox.dismiss();
      this.alertBox.present();
    }, 70000);

    if ( this.model.autoPosition ) {
      this.model.mlat = 0;
      this.model.mlng = 0;
    }

    this.newspringsService.saveSpring( this.model )
      .subscribe(
        data => {
          clearTimeout(this.timer);
          this.loaderBox.dismiss();
          this.alertBox.dismiss();
          this.showKey = 7;
        },
        error => {
          let alertError = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Erro ao enviar o material.' + error,
            buttons: [{
              text: 'OK',
              handler: data => {
                alertError.dismiss();
                this.cancelSend();
              }
            }]
          });
          clearTimeout(this.timer);
          this.loaderBox.dismiss();
          this.alertBox.dismiss();
          alertError.present();
        }
      );
  }

	getPhoto( source ){

		let options = {
			destinationType: 0,
			sourceType: source,
      encodingType: 0,
      quality: 50
		}
		this.camera.getPicture( options ).then(( imageData ) => {

      this.imgThumb = 'data:image/jpeg;base64,' + imageData;
      this.imgPreview = this.sanitizer.bypassSecurityTrustStyle( 'url(' + this.imgThumb + ')' );
      this.model.image = imageData;
      this.model.type = 'jpg';
      this.showKey =  2;

    }, ( error ) => {
      let alert = this.alertCtrl.create({
        title: 'Erro',
        subTitle: 'Erro ao processar a imagem.' + error,
        buttons: ['OK']
      });
      alert.present();
    });
  }

	springLocation() {
		this.storage.get( 'springLocation' ).then(( value ) => {
			if ( value ) {
        this.autoPos = false;
        this.ctmPos = true;
        this.model.autoPosition = false;
				this.model.mlat = value.lat;
				this.model.mlng = value.lng;
				this.locationMsg = 'Localização personalizada <br>lat: ' + this.model.mlat + '<br>' + 'lng: ' + this.model.mlng;
			} else {
        this.model.autoPosition = true;
        this.autoPos = true;
        this.ctmPos = false;
				this.locationMsg = 'Localização atual';
			}
		});
	}

	openMenu() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Escolha uma opção',
			cssClass: 'action-sheets-basic-page',
			buttons: [
				{
					text: 'Camera',
					icon: 'camera',
					handler: () => {
						this.getPhoto(1);
					}
				},
				{
					text: 'Galeria',
					icon: 'images',
					handler: () => {
						this.getPhoto(0);
					}
				},
				{
					text: 'Cancelar',
					role: 'cancel',
					icon: 'close',
					handler: () => {

					}
				}
			]
		});
		actionSheet.present();
	}

	goLogin() {
		this.navCtrl.push(LoginComponent);
	}

  prevStep() {
    this.showKey = this.showKey - 1;
  }

  nextStep(f) {
    if (this.showKey >= 3 && !f.valid) {
      let alert = this.alertCtrl.create({
        title: 'Erro',
        message: 'Todos os campos são obrigatórios',
        buttons: ['OK']
      });
      alert.present();
      return
    } else {
      this.showKey = this.showKey + 1;
      this.content.scrollToTop();
      return
    }

  }

  goCustonLocation( e: any ) {
    if( e.value ) {
      this.navCtrl.push( this.custonLocation );
      this.ctmPos = true;
      this.autoPos = false;
    }
  }

  goAutoLocation( e: any ) {
    if( e.value ) {
      this.ctmPos = false;
      this.rePosition();
      this.storage.get( 'springLocation' ).then(( value ) => {
        if ( value ) {
          this.storage.remove( 'springLocation' );
        }
      });
    } else {
      if( !this.ctmPos ) {
        this.ctmPos = true;
        this.navCtrl.push( this.custonLocation );
      }
    }
  }

  rePosition() {
    this.locationService.location().then(
      ( value ) => {
        this.locationPos = value;
      },
      error => {
        let alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'Erro ao configurar sua posição',
          buttons: ['OK']
        });
        alert.present();
      }
    )
  }

  cancelSend(){
    this.navCtrl.popToRoot();
    clearTimeout(this.timer);
  }
}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { PrivacypolicyComponent } from '../../legaltexts/privacypolicy-component/privacypolicy.component';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutComponent {

  privacyPolicy = PrivacypolicyComponent;

  constructor( public navCtrl: NavController ) {}

	ngOnInit(){}

}

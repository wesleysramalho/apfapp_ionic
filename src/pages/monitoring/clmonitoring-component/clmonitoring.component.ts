import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';

import { LocationService } from '../../../app/services/location.service';
import { LocationModel } from '../../../app/models/location.model';

@Component({
  templateUrl: 'clmonitoring.html'
})
export class ClmonitoringComponent implements OnInit {

  // Google Map zoom level
  zoom: number = 14;

  // Google Map center
  latitude: number;
  longitude: number;

  markers = {
    lat: this.latitude,
    lng: this.longitude,
    draggable: true,
    icon: 'assets/img/icon_water.png'
  };

  locationPos = <LocationModel>{};

	constructor (
    public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,
    public navCtrl: NavController,
    public locationServ: LocationService,
    private storage: Storage,
    private  geo: Geolocation
  ) {}

	ngOnInit() {
    // let loader = this.loadingCtrl.create({
    //   content: "Aguarde...",
    //   duration: 5000
    // });
    // loader.present();

    // this.storage.get('')

    // Geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(
    //   ( position ) => {
    //     this.markers.lat = position.coords.latitude;
    //     this.markers.lng = position.coords.longitude;
    //     this.latitude = position.coords.latitude;
    //     this.longitude = position.coords.longitude;
    //   }
    // );
    this.locationServ.location().then(
      ( location ) => {
        this.locationPos = location;
        this.markers.lat = this.locationPos.lat;
        this.markers.lng = this.locationPos.lng;
        this.latitude = this.locationPos.lat;
        this.longitude = this.locationPos.lng;
      }
    );
  }

  mapClicked( $event ) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.markers.lat = $event.coords.lat;
    this.markers.lng = $event.coords.lng;
  }

  markerDragEnd( m, $event ) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.markers.lat = $event.coords.lat;
    this.markers.lng = $event.coords.lng;
  }

  saveLocation() {
    let springLocation = {
      lat: this.markers.lat,
      lng: this.markers.lng
    }
    this.storage.set( 'springLocation', springLocation ).then(
      () => {
        this.navCtrl.pop();
      }
    );
  }

  currentLocation() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    this.geo.getCurrentPosition({ enableHighAccuracy: true }).then(
      ( position ) => {
        this.markers.lat = position.coords.latitude;
        this.markers.lng = position.coords.longitude;
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        loader.dismiss();
      }
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services/google-maps-api-wrapper';
import { MarkerManager } from '@agm/core/services/managers/marker-manager';

import { MonitoringComponent } from './monitoring-component/monitoring.component';
import { ClmonitoringComponent } from './clmonitoring-component/clmonitoring.component';

import { MonitoringService } from '../../app/services/monitoring.service';

// Mask directive
import { Ionic2MaskDirective } from'../../app/shared/mask.directive';
// import { Ionic2MaskDirective } from "ionic2-mask-directive";

@NgModule({
  declarations: [
    MonitoringComponent,
    ClmonitoringComponent,
    Ionic2MaskDirective
  ],
  imports: [
  	CommonModule,
  	SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'SEU_APP_KEY_AQUI'
    })
  ],
  exports: [
    MonitoringComponent,
    ClmonitoringComponent
  ],
  entryComponents:[
  	MonitoringComponent,
    ClmonitoringComponent
  ],
  providers: [
    MonitoringService,
    GoogleMapsAPIWrapper,
    MarkerManager
  ]
})
export class MonitoringModule {}

import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController, Platform } from 'ionic-angular';
import { AuthenticationService } from '../../../app/services/authentication.service';

import { HomeComponent } from '../../home/home-component/home.component';

@Component({
  selector: 'recover-login',
  templateUrl: './recover.html'
})
export class RecoverComponent implements OnInit {
  model: any = {};
  returnUrl: string;

  dataVar: any;
  codeVar: any;
  passVar: any;
  restoreCode: string;
  checkcode = false;
  passInput = false;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() { }

  getResetCode() {

    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });

    loader.present();

    this.authenticationService.resetCode(this.model.username).subscribe(
      response => this.dataVar = response,
      error => {
        let alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'erro ao enviar o pedido' + error,
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      },
      () => {
        if(this.dataVar.error == false && this.dataVar.params.id != ''){
          loader.dismiss();
          this.checkcode = true;
        } else {
          let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'e-mail não cadastrado',
            buttons: ['OK']
          });
          loader.dismiss();
          alert.present();
        }
      });
  }

  checkResetCode() {

    let loader = this.loadingCtrl.create({
      content: "Verificando código..."
    });

    loader.present();

    this.authenticationService.checkCode(this.model.username, this.model.code).subscribe(
      response => this.codeVar = response,
      error => {
        let alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'erro ao enviar o código' + error,
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      },
      () => {
        if(this.codeVar.error == false && this.codeVar.params.id != ''){
          loader.dismiss();
          this.model.name = this.codeVar.params.name;
          this.model.id = this.codeVar.params.id;
          this.checkcode = true;
          this.passInput = true;
        } else {
          let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'código não encontrado',
            buttons: ['OK']
          });
          loader.dismiss();
          alert.present();
        }
      });
  }

  checkPassword() {
    if (this.model.password != this.model.password2) {
      let alert = this.alertCtrl.create({
        title: 'Erro',
        subTitle: 'Senhas estão diferente',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.resetPassword();
    }
  }

  resetPassword() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });

    loader.present();

    this.authenticationService.resetPassword(this.model.id, this.model.password).subscribe(
      response => this.passVar = response,
      error => {
        let alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'erro ao enviar a senha' + error,
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      },
      () => {
        if(this.passVar.error == false){
          let alert = this.alertCtrl.create({
            title: 'Sucesso',
            subTitle: 'Senha salva com sucesso',
            buttons: [
              {
                text: 'Ok',
                handler: () => {
                  this.goHome();
                }
              }
            ]
          });
          loader.dismiss();
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Erro ao salvar a senha',
            buttons: ['OK']
          });
          loader.dismiss();
          alert.present();
        }
      });
  }

  goHome() {
    this.navCtrl.setRoot(HomeComponent);
  }

}

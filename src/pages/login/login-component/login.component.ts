import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController, MenuController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';

import { UserService  } from '../../../app/services/user.service';
import { AuthenticationService  } from '../../../app/services/authentication.service';

import { RecoverComponent } from '../../login/recover-component/recover.component';
import { RegisterComponent } from '../../register/register-component/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.html'
})
export class LoginComponent implements OnInit {
  model: any = {};
  returnUrl: string;

  dataVar:any;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private storage: Storage,
    private menu: MenuController,
    private fb: Facebook
  ) {}

  ngOnInit() { }

  login() {

    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });

    loader.present();

    this.authenticationService.login(this.model.username, this.model.password).subscribe(
      response => this.dataVar = response,
      error => {
        let alert = this.alertCtrl.create({
          title: 'Erro',
          subTitle: 'erro ao realizar o login' + error,
          buttons: ['OK']
        });
        loader.dismiss();
        alert.present();
      },
      () => {
        if(this.dataVar.error == false && this.dataVar.params.code != ''){
          let userObj = {
            "id": this.dataVar.params.code,
            "name": this.dataVar.params.name,
            "email": this.dataVar.params.email
          }
          this.storage.set('currentUser', userObj);
          this.userService.setAppUser(this.dataVar.params.name, this.dataVar.params.email);

          loader.dismiss();
          // this.menu.enable(true);
          this.navCtrl.pop();
        } else {
          let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Usuário ou senha incorretos',
            buttons: ['OK']
          });
          loader.dismiss();
          alert.present();
        }
      });
  }

  goRegister() {
    this.navCtrl.push(RegisterComponent);
  }

  fbLogin() {

    let alertStart = this.alertCtrl.create({
      title: 'Aguarde',
      subTitle: 'Conectando com o facebook..',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel',
        handler: data => {
          this.navCtrl.popToRoot();
        }
      }]
    });
    alertStart.present();

    this.platform.ready().then(() => {
      //FB authentication
      this.fb.login(["email","public_profile"]).then((result) => {

        //Call FB API to get user name and email
        this.fb.api("/me?fields=name,email", null)
          .then((success)=> {

              // ///Call API to check if user exist, if not create user and return authenticated data
              this.authenticationService.fbLogin(success.email, success.name).subscribe(
                response => this.dataVar = response,
                error => {
                  alertStart.dismiss();
                  let alert = this.alertCtrl.create({
                    title: 'Erro',
                    subTitle: 'erro ao realizar o login' + error,
                    buttons: ['OK']
                  });
                  // loader.dismiss();
                  alert.present();
                },
                () => {
                  if(this.dataVar.error == false && this.dataVar.params.code != ''){
                    let userObj = {
                      "id": this.dataVar.params.code,
                      "name": this.dataVar.params.name,
                      "email": this.dataVar.params.email
                    }
                    this.storage.set('currentUser', userObj);
                    this.userService.setAppUser(this.dataVar.params.name, this.dataVar.params.email);

                    this.menu.enable(true);
                    alertStart.dismiss();

                    this.navCtrl.popToRoot();
                  } else {
                    let alert = this.alertCtrl.create({
                      title: 'Erro',
                      subTitle: 'Usuário ou senha incorretos',
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                });


          }, function (error) {
            console.log("error", error);
        });
      });
    })
  }

  goRecover() {
    this.navCtrl.push(RecoverComponent);
  }

}

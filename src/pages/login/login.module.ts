import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { LoginComponent } from './login-component/login.component';
import { RecoverComponent } from './recover-component/recover.component';

@NgModule({
  declarations: [
    LoginComponent,
    RecoverComponent
  ],
  imports: [
  	CommonModule,
  	SharedModule
  ],
  exports: [
    LoginComponent,
    RecoverComponent
  ],
  entryComponents:[
  	LoginComponent,
    RecoverComponent
  ]
})
export class LoginModule {}

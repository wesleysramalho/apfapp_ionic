import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SpringsComponent } from '../../springs/springs-component/springs.component';
import { NewspringsComponent } from '../../newspring/newspring-component/newspring.component';
import { MonitoringComponent } from '../../monitoring/monitoring-component/monitoring.component';
import { ImagerecognitionComponent } from '../../imagerecognition/imagerecognition-component/imagerecognition.component';

import { LocationModel } from '../../../app/models/location.model';
import { StatusBar } from '@ionic-native/status-bar';
// typical import
import {TweenLite, Power2, TimelineLite} from 'gsap';
import * as DrawSVGPlugin from '../../../assets/plugins/DrawSVGPlugin.min';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomeComponent {

  locationPos = <LocationModel>{};
  cityCheck = true;
  cityCheckRes: any;

  tl = new TimelineLite();
  @ViewChild('logoHomeSVG') private logoHome: ElementRef;
  @ViewChild('dropManSVG') private dropMan: ElementRef;
  @ViewChild('welcomeMSG') private welcome: ElementRef;
  @ViewChild('btnMap') private btnMap: any;
  @ViewChild('btnNewspring') private btnNewspring: any;
  @ViewChild('btnMonitoring') private btnMonitoring: any;
  @ViewChild('btnAr') private btnAr: any;

  constructor(
    public navController: NavController,
    private sbar: StatusBar
  ) {}

  ngOnInit() {
    console.log(this.logoHome)

    window.setTimeout(() => {
      this.animateHome();
    }, 500);
  }

  gotoPage( target ) {
    switch( target ){
      case "nascente":
        this.navController.push( SpringsComponent );
        break;
      case "nova_nascente":
        this.navController.push( NewspringsComponent );
        break;
      case "monitoramento":
        this.navController.push( MonitoringComponent );
        break;
      case "ar":
        this.navController.push( ImagerecognitionComponent );
        break;
    }
  }

  ionViewWillEnter() {
    this.sbar.styleBlackTranslucent();
  }

  ionViewWillLeave() {
    this.sbar.styleDefault();

  }

  ionViewDidEnter() {

  }

  private animateHome() {
    this.tl
    .to(this.logoHome.nativeElement, .75, {
        opacity: 1,
        transform: 'translate(0px, 0px) scale(1, 1)',
        ease: Power2.easeOut
      }
    )
    .to(this.dropMan.nativeElement, .5, {
        opacity: 1,
        transform: 'translate(0px, 0px) scale(1, 1)',
        ease: Power2.easeOut
      }
    )
    .to(this.welcome.nativeElement, .5, {
        opacity: 1,
        transform: 'translate(0px, 0px)',
        ease: Power2.easeOut
      }
    );

    TweenLite
    .to(this.btnMap._elementRef.nativeElement, .5, {
        opacity: 1,
        transform: 'translate(0px, 0px) scale(1, 1)',
        ease: Power2.easeOut
      }
    );
    TweenLite
    .to(this.btnNewspring._elementRef.nativeElement, .5, {
        opacity: 1,
        transform: 'translate(0px, 0px) scale(1, 1)',
        ease: Power2.easeOut
      }
    );
    TweenLite
    .to(this.btnMonitoring._elementRef.nativeElement, .5, {
        opacity: 1,
        transform: 'translate(0px, 0px) scale(1, 1)',
        ease: Power2.easeOut
      }
    );
    TweenLite
    .to(this.btnAr._elementRef.nativeElement, .5, {
        opacity: 1,
        transform: 'translate(0px, 0px) scale(1, 1)',
        ease: Power2.easeOut
      }
    );
  }

}

interface DrawSVGPlugin {

}

declare var DrawSVGPlugin:DrawSVGPlugin;

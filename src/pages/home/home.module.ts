import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { HomeComponent } from './home-component/home.component';

import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    IonicStorageModule.forRoot(),
  	CommonModule,
  	SharedModule
  ],
  exports: [
    HomeComponent
  ],
  entryComponents:[
  	HomeComponent
  ]
})
export class HomeModule {}

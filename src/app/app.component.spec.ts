import { TestBed, async } from '@angular/core/testing';
import { IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Diagnostic } from '@ionic-native/diagnostic';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { StorageMock } from './services/storage.mock';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBarMock, SplashScreenMock } from 'ionic-mocks';
import { TranslateServiceMock } from './services/translate.mock';
import { TranslatePipeMock } from './pipes/translate.pipe.mock';
import { LocationServiceMock } from './services/location.service.mock';
import { LocationService } from './services/location.service';
import { ServerServiceMock } from './services/server.service.mock';
import { ServerService } from './services/server.service';
import { UserServiceMock } from './services/user.service.mock';
import { UserService } from './services/user.service';

let fixture = null;
let app = null;

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule.forRoot(MyApp)
      ],
      declarations: [
        MyApp,
        TranslatePipeMock
      ],
      providers: [
        { provide: TranslateService, useClass: TranslateServiceMock },
        { provide: LocationService, useClass: LocationServiceMock },
        { provide: UserService, useClass: UserServiceMock },
        { provide: ServerService, useClass: ServerServiceMock },
        { provide: Storage, useClass: StorageMock },
        { provide: SplashScreen, useClass: SplashScreenMock },
        { provide: StatusBar, useClass: StatusBarMock },
        Diagnostic
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MyApp);
    app = fixture.debugElement.componentInstance;

    app.nav = (<any>{});
    app.nav.setRoot = (<any>(() => true));
    spyOn(app.nav, 'setRoot');
  }));
  it('should create the app', async(() => {
    expect(app).toBeTruthy();
  }));

  it('initialises with a root page', () => {
    expect(app['rootPage']).not.toBe(null);
  });

});


//let instance: MyApp = null;

// describe('ClickerApp', () => {

//   beforeEach(() => {
//     instance = new MyApp(
//       (<any> PlatformMock.instance()),
//       (<any>new TranslateServiceMock()),
//       (<any> AlertControllerMock.instance()),
//       (<any> LoadingControllerMock.instance()),
//       (<any> LocationServiceMock),
//       (<any> StorageMock.instance()),
//       (<any> MenuMock.instance()),
//       (<any> UserServiceMock),
//       (<any> ServerServiceMock),
//       (<any> Diagnostic),
//       (<any>SplashScreenMock.instance()),
//       (<any>StatusBarMock.instance())
//     );

//     // ionic-mocks have lost the nav mock
//     instance.nav = (<any>{});
//     instance.nav.setRoot = (<any>(() => true));
//     spyOn(instance.nav, 'setRoot');
//   });

//   it('initialises with a root page', () => {
//     expect(instance['rootPage']).not.toBe(null);
//   });

// });

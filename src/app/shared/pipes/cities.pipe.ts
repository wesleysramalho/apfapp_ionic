import { Pipe, PipeTransform } from '@angular/core';
import { CitiesModel } from '../../models/cities.model'

@Pipe({
	name: 'citiesFilter'
})

export class CitiesPipe implements PipeTransform {  
 transform(cities: CitiesModel[], term: any): any {
		if(term === undefined) return cities;
		return cities.filter(function(city){
				return city.city.toLowerCase().includes(term.toLowerCase());
		});
	}
} 
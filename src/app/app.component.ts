import { AboutComponent } from '../pages/about/about-component/about.component';
import {
  AlertController,
  LoadingController,
  MenuController,
  Nav,
  Platform
  } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { ContactComponent } from '../pages/contact/contact-component/contact.component';
import { HomeComponent } from '../pages/home/home-component/home.component';
import { LocationModel } from './models/location.model';
import { LoginComponent } from '../pages/login/login-component/login.component';
import { MonitoringComponent } from '../pages/monitoring/monitoring-component/monitoring.component';
import { NewspringsComponent } from '../pages/newspring/newspring-component/newspring.component';
import { PartnersComponent } from '../pages/partners/partners-component/partners.component';
import { ProjectComponent } from '../pages/project/project-component/project.component';
import { ImagerecognitionComponent } from '../pages/imagerecognition/imagerecognition-component/imagerecognition.component';
import { ServerService } from './services/server.service';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpringsComponent } from '../pages/springs/springs-component/springs.component';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from './services/user.service';

@Component({
	templateUrl: './app.html',
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;

	rootPage = HomeComponent;
	pages: Array<{title: string, component: any, icon: string}>;
	userName:any;
	userEmail:any;

	locationPos = <LocationModel>{};
  cityCheckRes: any;
  srvVar: any;

	constructor(
		public platform: Platform,
		public translate: TranslateService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private menu: MenuController,
    private userService: UserService,
    private serverService: ServerService,
    private splash: SplashScreen
		) {

			translate.setDefaultLang('pt-br');
			storage.get( 'language' ).then(
        ( value ) => {
  				if ( value ) {
  					translate.use( value );
  				} else {
  					translate.use( 'pt-br' );
  					storage.set( 'language', 'pt-br' );
  				}
        }
      );

			this.pages = [
				{ title: 'SPRINGS', component: SpringsComponent, icon: 'apf-map' },
				{ title: 'NEW_SPRING', component: NewspringsComponent, icon: 'newspring' },
				{ title: 'MONITORING', component: MonitoringComponent, icon: 'monitoring' },
				{ title: 'IMAGERECOGNITION', component: ImagerecognitionComponent, icon: 'images' },
				{ title: 'ABOUT_PROJECT', component: ProjectComponent, icon: 'apf' },
				{ title: 'PARTNERS', component: PartnersComponent, icon: 'people-group' },
				{ title: 'ABOUT', component: AboutComponent, icon: 'info-outline' },
				{ title: 'CONTACT', component: ContactComponent, icon: 'mail-outline2' }
      ];

      this.initializeApp();
	}

	initializeApp() {

    this.platform.ready().then(() => {

      // if user information is found in the storage redirect to Home
      this.storage.get( 'currentUser' ).then(( value ) => {
        if ( value ) {
          this.userService.setAppUser( value.name, value.email );
          this.menu.enable( true );
          this.nav.setRoot( HomeComponent );
        }
      });

      this.splash.hide();
    });

    this.userService.name$.subscribe(
      userName => this.userName = userName
    );
    this.userService.email$.subscribe(
      userEmail => this.userEmail = userEmail,
    );

    let alert = this.alertCtrl.create({
      title: 'Erro',
      buttons: [{
        text: 'Fechar',
        handler: () => {
            this.platform.exitApp();
          }
        }]
    });

		this.serverService.serverStatus().subscribe(
			response => this.srvVar = response,
			error => {
				alert.setMessage('Não foi possível conectar com o servidor. Tente novamente mais tarde.');
				alert.present();
			},
			() => {
				if(this.srvVar) {
          if(this.srvVar.error) {
            alert.setMessage('Falha na resposta do servidor. Tente novamente mais tarde.');
            alert.present();
          }
				}
			}
    );
  }

	openPage(page) {
		this.nav.push( page.component );
	}

	goLogin() {
		this.menu.close();
		this.nav.push( LoginComponent );
	}

	logOut() {
		this.storage.remove( 'currentUser' );
		delete this.userName, this.userEmail;
		this.menu.close();
		this.nav.popToRoot();
  }

}

'use strict';

export class StorageMock {

  public get(key: string): Promise<{}> {
    let rtn: string = null;

    return new Promise((resolve: Function) => {
      resolve(rtn);
    });
  }

  public set(key: string, value: string): Promise<{}> {
    return new Promise((resolve: Function) => {
      resolve({key: key, value: value});
    });
  }

  public remove(key: string): Promise<{}> {
    return new Promise((resolve: Function) => {
      resolve({key: key});
    });
  }
}

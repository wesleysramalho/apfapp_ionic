import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class SpringsService {
    constructor(
        private http: Http,
        @Inject(EnvVariables) public envVar
      ) { }

    private _urlapi = this.envVar.url + this.envVar.api;

    public getSprings(): Observable<any> {
        return this.http.get(this._urlapi+'?Op=findSprings').map(
            (res: Response) => res.json() || {}
        );
    }

    public springDetail(id){
      return this.http.get(this._urlapi+'?Op=findOneSpring&id='+id).map(
          (res: Response) => res.json() || {}
      );
    }

    public setShapes(){
      return this.http.get(this._urlapi+'?Op=setShapes').map(
        (res: Response) => res.json() || {}
      );
    }
}

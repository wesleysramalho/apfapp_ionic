import { Injectable, Inject } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { EnvVariables } from '../environment-variables/environment-variables.token';
import { NewSpringModel } from '../models/spring.model';

@Injectable()
export class NewspringsService {
  constructor(
    private http: Http,
    @Inject(EnvVariables) public envVar
  ) {}

  private _urlapi = this.envVar.url + this.envVar.api;

  saveSpring( model: NewSpringModel ) {

    const body = new URLSearchParams();
    let headers = new Headers();

    // New Spring Object
    body.set('image', model.image);
    body.set('type', model.type);
    body.set('observation', model.observation);
    body.set('description', model.description);
    // user System Info
    body.set('userId', model.userId.toString());
    body.set('userName', model.userName);
    body.set('userEmail', model.userEmail);
    // Location
    body.set('autoposition', model.autoPosition.toString());
    body.set('lat', model.lat.toString());
    body.set('lng', model.lng.toString());
    body.set('mlat', model.mlat.toString());
    body.set('mlng', model.mlng.toString());
    // Contact Info
    body.set('name', model.name);
    body.set('email', model.email);
    body.set('phone', model.phone);
    body.set('incognito', model.incognito.toString());

    headers.append( 'Content-Type', 'application/x-www-form-urlencoded' );

    return this.http.post( this._urlapi+'?Op=createSpring', body.toString(), { headers : headers }).map(
      ( response: Response ) => response || {}
    );

  }
}

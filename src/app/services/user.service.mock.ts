import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';

export class UserServiceMock {

    // Observable string sources
    private nameSource = new Subject<string>();
    private emailSource = new Subject<string>();
    // Observable string streams
    name$ = this.nameSource.asObservable();
    email$ = this.emailSource.asObservable();

    setAppUser(name: string, email: string) {
        this.nameSource.next(name);
        this.emailSource.next(email);
    }

    create(user: any): Observable<any> {
      return Observable.of(user);
    }

}

import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class ServerService {

    constructor(
      private http: Http,
      @Inject(EnvVariables) public envVar
    ) {}

    public serverStatus(){
        return this.http.get(this.envVar.url + this.envVar.api + '?Op=status').map(
            (res: Response) => res.json() || {}
        );
    }
}

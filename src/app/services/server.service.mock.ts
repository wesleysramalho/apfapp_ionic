import { Observable } from 'rxjs';

export class ServerServiceMock {

    public serverStatus(): Observable<any> {
      return Observable.of();
    }
}

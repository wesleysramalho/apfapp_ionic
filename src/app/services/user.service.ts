import { Inject, Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class UserService {

    // Observable string sources
    private nameSource = new Subject<string>();
    private emailSource = new Subject<string>();
    // Observable string streams
    name$ = this.nameSource.asObservable();
    email$ = this.emailSource.asObservable();
    // Service set user variables to set side menu

    private _urlapi = this.envVar.url + this.envVar.api;

    constructor(
        private http: Http,
        @Inject(EnvVariables) public envVar
      ){}

    setAppUser(name: string, email: string) {
        this.nameSource.next(name);
        this.emailSource.next(email);
    }

    create(user: any) {

        const body = new URLSearchParams();
        let headers = new Headers();

        body.set('name', user['name']);
        body.set('username', user['email']);
        body.set('email', user['email']);
        body.set('password', user['password']);
        body.set('gender', 'm');

        headers.append('Content-Type','application/x-www-form-urlencoded');

        return this.http.post(this._urlapi+'?Op=createUser', body.toString(), { headers : headers }).map(
            (res: Response) => res.json() || {}
        );

    }

    // private helper methods

    // private une() {
    //     // create authorization header with une token
    //     let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //     if (currentUser && currentUser.token) {
    //         let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
    //         return new RequestOptions({ headers: headers });
    //     }
    // }
}

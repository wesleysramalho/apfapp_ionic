import { Observable } from 'rxjs';

import { CitiesModel } from '../../app/models/cities.model';
import { LocationModel } from '../../app/models/location.model';

export class LocationServiceMock {
  locationPos = <LocationModel>{};
  citiesRes: any;
  cities: CitiesModel[] = [];

  public getCities(): Observable<any> {
    return Observable.of();
  }

	public checkCity(city: string): Observable<any> {
		return Observable.of(city);
	}

	public setLocation(options = <LocationModel>{}): any {
    return Observable.of(options);
  }

	public getCityCenter(city: string): any {
		return Observable.of(city);
  }

  public location(): void {}

  public getAddressByZipCode(zipcode: number): void {}

}

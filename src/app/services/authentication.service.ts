import { Injectable, Inject } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class AuthenticationService {

    private _urlapi = this.envVar.url + this.envVar.api;

    constructor(
        private http: Http,
        @Inject(EnvVariables) public envVar
      ){}

    public login(email: string, pass: string) {

        const body = new URLSearchParams();
        let headers = new Headers();

        body.set('email', email);
        body.set('password', pass);

        headers.append('Content-Type','application/x-www-form-urlencoded');

        return this.http.post(this._urlapi+'?Op=login', body.toString(), { headers : headers }).map(
            (res: Response) => res.json() || {}
        );

    }

    public fbLogin(userEmail: string, userName: string): Observable<any> {

        const body = new URLSearchParams();
        let headers = new Headers();

        body.set('userEmail', userEmail);
        body.set('userName', userName);

        headers.append('Content-Type','application/x-www-form-urlencoded');

        return this.http.post(this._urlapi+'?Op=fblogin', body.toString(), { headers : headers }).map(
          (res: Response) => res.json() || {}
        );
    }

    public resetCode(email: string) {

        const body = new URLSearchParams();
        let headers = new Headers();

        body.set('email', email);

        headers.append('Content-Type','application/x-www-form-urlencoded');

        return this.http.post(this._urlapi+'?Op=resetcode', body.toString(), { headers : headers }).map(
            (res: Response) => res.json() || {}
        );

    }

    public checkCode(email: string, code: string) {

        const body = new URLSearchParams();
        let headers = new Headers();

        body.set('email', email);
        body.set('code', code);

        headers.append('Content-Type','application/x-www-form-urlencoded');

        return this.http.post(this._urlapi+'?Op=checkcode', body.toString(), { headers : headers }).map(
            (res: Response) => res.json() || {}
        );
    }

    public resetPassword(id: string, password: string) {

        const body = new URLSearchParams();
        let headers = new Headers();

        body.set('id', id);
        body.set('password', password);

        headers.append('Content-Type','application/x-www-form-urlencoded');

        return this.http.post(this._urlapi+'?Op=resetpassword', body.toString(), { headers : headers }).map(
            (res: Response) => res.json() || {}
        );
    }
}

import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import {
    NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult
} from '@ionic-native/native-geocoder';

import { AlertController, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { CitiesModel } from '../../app/models/cities.model';
import { LocationModel } from '../../app/models/location.model';
import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class LocationService {
  locationPos = <LocationModel>{};
  citiesRes: any;
  cities: CitiesModel[] = [];

  constructor(
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private nativeGeocoder: NativeGeocoder,
    private geolocation: Geolocation,
    private http: Http,
    @Inject(EnvVariables) public envVar
  ) {}

  private _urlapi = this.envVar.url + this.envVar.api;

  public getCities(): Observable<any> {
    return this.http.get( this._urlapi + '?Op=citiesmt' ).map(
      ( res: Response ) => res.json() || {}
    );
  }

	public checkCity( city: string ): Observable<any> {
		return this.http.get( this._urlapi + '?Op=checkcity&city=' + city ).map(
			( res: Response ) => res.json() || {}
		);
	}

	public setLocation( options = <LocationModel>{} ): any {

    let loader = this.loadingCtrl.create({
      content: "configurando sua localização...",
      dismissOnPageChange: true
    });

    loader.present();
    setTimeout(() => {
      loader.dismiss();
    }, 10000);

    return new Promise(( resolvePosition, rejectPosition ) => {

      // get current position cordinates and set to options
  		this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(
  			( position ) => {
  				options.lat = position.coords.latitude;
          options.lng = position.coords.longitude;

            let resPos = new Promise(( resolseReverse, rejectReverse ) => {

              // get address from current position cordinates
              this.nativeGeocoder.reverseGeocode( options.lat, options.lng ).then(
                (result: NativeGeocoderReverseResult) => {
                  options.administrativeArea = result.administrativeArea;
                  options.city = result.subAdministrativeArea;

                    let resRev = new Promise(( resolveCity, rejectCity ) => {
                      if ( options.administrativeArea == 'Mato Grosso' ) {
                        options.auto = true;

                        // check city
                        this.checkCity( options.city ).subscribe(
                          ( status ) => {
                            options.citystatus = status.error ? false : true;
                            loader.dismiss();
                            resolveCity( options);
                          },
                          ( error ) => rejectPosition( error )
                        );
                      } else {
                        options.auto = false;
                        options.city = 'Cuiabá';
                        options.citystatus = false;
                        loader.dismiss();
                        resolveCity( options );
                      }
                    });

                  resolseReverse( resRev );

                }
              ).catch((error) => rejectPosition( error ) );
            });

          resolvePosition(resPos);

        }
      ).catch((error) => rejectPosition( error ));
      });
  }

	public getCityCenter( city:string ): any {
		return this.nativeGeocoder.forwardGeocode( city ).then(
			( location: NativeGeocoderForwardResult ) => location
		).catch(
			( error: any ) => console.log( error )
		);
  }

  public location(): any  {

    return new Promise((resolve, reject) => {
      this.diagnostic.getLocationAuthorizationStatus().then(
        (status) => {

          const statusCheck = /granted|when_in_use|authorized_when_in_use|always/.test(status.toLowerCase());

          if (!statusCheck) {
            let alertLocation = this.alertCtrl.create({
              title: 'Localização',
              subTitle: 'Precisamos da sua localização para configurar o aplicativo e localizar as nascentes próximas a você.',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'OK',
                  handler: data => {
                    this.setLocation().then(
                      ( res ) => resolve( res )
                    ).catch((error) => reject( error ));
                  }
                }
              ]
            });
            alertLocation.present();
          } else {
            this.setLocation().then(
              ( res ) => resolve( res )
            );
          }
        })
    })

  }

  public getAddressByZipCode( zipcode: number ) {
    let postmonApi = 'http://api.postmon.com.br/v1/cep/';
    return this.http.get( postmonApi + zipcode )
      .map(( response: Response ) => response.json() );
  }

}

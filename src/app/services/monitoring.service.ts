import { Injectable, Inject } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { EnvVariables } from '../environment-variables/environment-variables.token';
import { MonitoringModel } from '../models/spring.model';

@Injectable()
export class MonitoringService {
    constructor(
        private http: Http,
        @Inject(EnvVariables) public envVar
      ) { }

    private _urlapi = this.envVar.url + this.envVar.api;

    sendMessage(model: MonitoringModel) {

        const body = new URLSearchParams();
        let headers = new Headers();

        // monitoring massage
        body.set( 'assunto', model.subject.toString() );
        body.set( 'image', model.image );
        body.set( 'msg', model.message );
        // User System info
        body.set( 'userEmail', model.userEmail );
        body.set( 'userName', model.userName );
        body.set( 'userId', model.userId.toString() );
        body.set( 'citystatus', 'true' );
        // Location
        body.set( 'autoposition', model.autoPosition.toString());
        body.set( 'lat', model.lat ? model.lat.toString() : '');
        body.set( 'lng', model.lng ? model.lng.toString() : '');
        body.set( 'mlat', model.mlat ? model.mlat.toString() : '');
        body.set( 'mlng', model.mlng ? model.mlng.toString() : '');
        // Personal info
        body.set( 'name', model.name );
        body.set( 'email', model.email );
        body.set( 'phone', model.phone );
        body.set( 'incognito', model.incognito.toString() );
        // Address
        body.set( 'zipcode', model.zipcode );
        body.set( 'state', model.state );
        body.set( 'street', model.street );
        body.set( 'number', model.number );
        body.set( 'city', model.city );
        body.set( 'district', model.district );

        headers.append( 'Content-Type', 'application/x-www-form-urlencoded' );

        return this.http.post( this._urlapi+'?Op=monitoring', body.toString(), { headers : headers }).map(
            (res: Response) => res || {}
        );

    }
}

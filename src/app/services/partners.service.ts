import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class PartnersService {
    constructor(
        private http: Http,
        @Inject(EnvVariables) public envVar
      ) { }

    private _urlapi = this.envVar.url + this.envVar.api;

    public getPartners(): Observable<any> {
        return this.http.get(this._urlapi+'?Op=partners').map(
            (res: Response) => res.json() || {}
        );
    }
}

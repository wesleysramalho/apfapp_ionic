import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { EnvVariables } from '../environment-variables/environment-variables.token';

@Injectable()
export class LegaltextService {
    constructor(
        private http: Http,
        @Inject(EnvVariables) public envVar
      ) {}

    private _urlapi = this.envVar.url + this.envVar.api;

    public getLegal(): Observable<any> {
        return this.http.get(this._urlapi+'?Op=legalText').map(
            (res: Response) => res.json() || {}
        );
    }
}

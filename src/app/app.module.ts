import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from './shared/shared.module'
import { HomeModule } from '../pages/home/home.module';
import { AboutModule } from '../pages/about/about.module';
import { ProjectModule } from '../pages/project/project.module';
import { ContactModule } from '../pages/contact/contact.module';
import { LoginModule } from '../pages/login/login.module';
import { RegisterModule } from '../pages/register/register.module';
import { SpringsModule } from '../pages/springs/springs.module';
import { NewspringsModule } from '../pages/newspring/newspring.module';
import { MonitoringModule } from '../pages/monitoring/monitoring.module';
import { LegaltextsModule } from '../pages/legaltexts/legaltexts.module';
import { PartnersModule } from '../pages/partners/partners.module';
import { ImagerecognitionModule } from '../pages/imagerecognition/imagerecognition.module';
import { Diagnostic } from '@ionic-native/diagnostic';
import { EnvironmentsModule } from './environment-variables/environment-variables.module';

//import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';

import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { ServerService } from './services/server.service';
import { LocationService } from './services/location.service';

import { CitiesPipe } from './shared/pipes/cities.pipe';

@NgModule({
  declarations: [
    MyApp,
    CitiesPipe
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      backButtonIcon: 'ios-arrow-back',
      iconMode: 'md'
    }),
    EnvironmentsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HomeModule,
    AboutModule,
    ProjectModule,
    ContactModule,
    LoginModule,
    RegisterModule,
    SpringsModule,
    NewspringsModule,
    MonitoringModule,
    LegaltextsModule,
    PartnersModule,
    ImagerecognitionModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthenticationService,
    UserService,
    ServerService,
    LocationService,
    Diagnostic,
    SplashScreen
  ]
})
export class AppModule {}

export interface MonitoringModel {
  userId: number;
  userName: string;
  userEmail: string;
  image: string;
  autoPosition: boolean;
  lat: number;
  lng: number;
  mlat: number;
  mlng: number;
  message: string;
  subject: string;
  citystatus: boolean;
  name: string;
  email: string;
  phone: string;
  zipcode: string;
  street: string;
  number: string;
  district: string;
  city: string;
  state: string;
  incognito: boolean;
}

export interface NewSpringModel {
  userId: number;
  userEmail: string;
  userName: string;
  incognito: boolean;
  name: string;
  phone: string;
  email: string;
  image: string;
  type: string;
  observation: string;
  description: string;
  autoPosition: boolean;
  lat: number;
  lng: number;
  mlat: number;
  mlng: number;
}

export interface SpringItem {
  id: number;
  title: string;
  featured: number;
  district: string;
  stream: string;
  icon: string;
  lat: number;
  lng: number;
  location: string;
  subbasin: string;
}

export interface shapesSettings {
  shapeWaterCourseLink: string;
  shapeWaterCourse: boolean;
  shapeWaterMassLinks: string;
  shapeWaterMass: boolean;
}

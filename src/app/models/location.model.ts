export interface LocationModel {
	lat: number;
	lng: number;
	city: string;
	auto: boolean;
  citystatus: boolean;
  administrativeArea: string;
}

export interface CurrentLatLng {
  lat: number;
  lng: number;
}
